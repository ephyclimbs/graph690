import base
import sys

graph = base.parse_dimacs(sys.argv[1])

def conected_components(graph):
	vtxs=set(sorted(graph.get_vtxs()))
	comps=[]
	while len(vtxs)>0:
		v=vtxs.pop()
		r=dfs(graph.get_graph(), v)
		vtxs=vtxs-r
		comps.append(r)
	return comps

if sys.argv[2]=='check_connectivity':
	print(graph.is_connected())

if sys.argv[2]=='dfs':
	queryVtx = sys.argv[3]
	print(base.dfs(graph.get_graph(), int(queryVtx)))

if sys.argv[2]=='bfs':
	queryVtx = sys.argv[3]
	print(base.bfs(graph.get_graph(), int(queryVtx)))

if sys.argv[2]=='connected_components':
	comps = conected_components(graph)
	for comp in comps:
		print(comp)