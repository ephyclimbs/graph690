#!/bin/bash

echo "****************Checking connectivity..."
ls data/test_graphs | xargs -i python src/python/hw2.py data/test_graphs/{} check_connectivity

echo "****************Vertex/Edge Count"
ls data/test_graphs | xargs -i head -1 data/test_graphs/{}

echo "*****************Running DFS's"
ls data/test_graphs | xargs -i time python src/python/hw2.py data/test_graphs/{} dfs 0 | grep elapsed

echo "*****************Running BFS's"
ls data/test_graphs | xargs -i time python src/python/hw2.py data/test_graphs/{} bfs 0 | grep elapsed