1.
	a) BruteForceEdgeConnectivity(G) (BFEC(G)) would reach the final return on k-connected graphs where k>1.
	b) Worst case behavior time behaviour would come from k-connected graphs where k>1 and where n is very large.
	c) If n=total vertices, T(G)=time for isConnected(G) and l(G)=minimum edge connectivity of G, what is the time for BFEC(G) as func of T(G) and l(G)?
		Roughly: T(G)*2 + T(G)*nCk, 0<r<=l(G)<n and k takes integer values 1,2,3...
		Therefor, BFEC(G) computes in n^k polynomial time plus an intercept
		*Just a note to Aaron: I am not a CS student and really don't know much about complexity, so please excuse me if my notation/detail work seem odd.

#'s 2,3,4 can be tested using run.sh
*For questions 2 and 3 I got help writing my depth and breadth-first searches from:
https://eddmann.com/posts/depth-first-search-and-breadth-first-search-in-python/
They show some nice alternative approaches, but I had to do a bit of rewriting because of an error in their code that caused a reccursion error.

5.
To log benchmarking results (since ubuntu's time outputs to stderr for some unknown reason):
./benchmark_searches.sh &> bm.log

Findings from benchmark data:
DFS almost always outperforms BFS. The cases where this is not true, the graph is also not fully connected and is not very large, which probably explains the algorithm's adbvantage. The time to compute these searches seems to scale linear to edges for DFS, but exponential to edges for BFS, so on large edge graphs there is a big difference. In both algorithms, the density and connectivity of the graph mitigates other relationships. I used my benchmarking log data in an R script that can be found in src/R/ to evaluate this. Obviously this is a very limited sample and could easily be tricking me, but the insights previously listed seem robust in theory as well as this practice. Other than the small sample of graphs, it's also easy to imagine how my implimentations of DFS and BFS could influence point estimates and scaling comparisons between the algorithms. I tried to program the searches as similarly as possible, but I'm no computer scientist so I could be biasing my results programticaly.
