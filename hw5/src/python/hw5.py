import base
import sys
import numpy as np
import itertools
import time

## Begin algo
def solve_stable(wgraph, evenodd):
	evens = dict.fromkeys([vtx for vtx in wgraph.graph.get_vtxs() if vtx%2==0])
	odds = dict.fromkeys([vtx for vtx in wgraph.graph.get_vtxs() if vtx%2!=0])

	wm = wgraph.get_weight_mat()
	wm[wm==0]=None

	for vtx in list(evens.keys()):
		evens[vtx]=[None]*len(odds)
		ranks=list(np.argsort(wm[vtx], ))
		for i,r in enumerate(ranks):
			if(r>=0):
				evens[vtx][r]=i

	for vtx in list(odds.keys()):
		odds[vtx]=[None]*len(evens)
		ranks=list(np.argsort(wm[vtx]))
		for i,r in enumerate(ranks):
			if(r>=0):
				odds[vtx][r]=i

	if(evenodd=='1'):
		s=evens
		m=odds
	else:
		m=odds
		s=evens

	# Initialize proposals
	proposals = []

	# Keep track of rejections
	rejects = {}
	for v in s:
		rejects[v]=[]

	allProps = False
	while not allProps:
		# Make proposals
		for k,prefs in s.items():
			if(k not in list(itertools.chain(*proposals))):
				prefs = [pref for pref in prefs if pref not in rejects[k] or pref in [a for a in list(m.keys()) if a not in list(itertools.chain(*proposals))]]
				pref = prefs[0]
				proposals.append((k, pref))

		# Make Rejections
		for k,prefs in m.items():
			theirProps = [prop for prop in proposals if k in prop]
			if(len(theirProps)>0):
				matches=[]
				for prop in theirProps:
					matches.append([p for p in prop if p!=k][0])
				keep = np.argmin(wm[k][matches])
				for prop in theirProps:
					if keep not in prop:
						rejects[[p for p in prop if p!=k][0]].append(k)
				proposals = list(filter(lambda x: k not in x , proposals))
				proposals.append((keep, k))

		allProps = len(proposals)==len(evens)
	return proposals


wgraph = base.parse_directed(sys.argv[1])
marriages = solve_stable(wgraph, sys.argv[2])
for marriage in marriages:
	print(marriage)




