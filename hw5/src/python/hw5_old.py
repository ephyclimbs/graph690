import base
import sys
import numpy as np


def is_stable_oneWay(marriageRec, wm, first):
	if first%2==0:
		f=0
		s=1
	else:
		f=1
		s=0
	for mInd, x in enumerate(marriageRec):
		for i, w in enumerate(wm[x[f]]):
			if(wm[x[f]][x[s]]>w and wm[x[f]][i]<wm[x[f]][x[s]]):
				return False, mInd, x[s], x[f]
	return True,None,None,None

def is_stable(marriageRec, wm):
	for mInd, x in enumerate(marriageRec):
		for i,w in enumerate(wm[x[1]]):
			if(wm[x[1]][x[0]]>w and wm[x[1]][i]<wm[x[1]][x[0]]):
				return False, mInd, x[0]
	return True,None,None

## Begin algo
def solve_stable(wgraph, evenodd):
	evens = [vtx for vtx in wgraph.graph.get_vtxs() if vtx%2==0]
	odds = [vtx for vtx in wgraph.graph.get_vtxs() if vtx%2!=0] 

	if evenodd=='0': 
		marriageRec=[(x, None) for x in evens]
		first=0
	if evenodd=='1': 
		marriageRec=[(x, None) for x in odds]
		first=1

	wm = wgraph.get_weight_mat()
	wm[wm==0]=None

	searching=True
	rejected=[]

	for mInd, x in enumerate(marriageRec):
		inds = [i for i,x in enumerate(wm[x[0]]) if i in [x[1] for x in marriageRec]]
		marriageRec[mInd]=(x[0], np.argmin(wm[x[0]].drop(inds)))

	stable,ind,rejectedS,rejectedF = is_stable_oneWay(marriageRec, wm, first)
	if stable:
		return marriageRec
	
	

	marriageRec[ind]=(None, None)

	first+=1


	return marriageRec


wgraph = base.parse_directed(sys.argv[1])
print(solve_stable(wgraph, sys.argv[2]))




