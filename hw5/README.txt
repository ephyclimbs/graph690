1. 
	(a) Can the dual graph of a plane graph G be itself a planar graph?
	Yes.
	(b) Is the dual graph of a plane graph G unique?
	Yes.
	(c) Is the dual graph of a planar graph G unique?
	No.
	(d) Can the dual graph of a planar graph G be isomorphic to G?
	Yes.
	(e) Can the line graph of a planar graph G be planar? If so, give an example.
	Yes, C4 is an example.
	(f) Can the line graph of a graph G be isomorphic to G? If so, give an example.
	Yes, C4 is also an example of this.

2. Unstable roommate preference list example:
	A:(E,B,F,C,D), B:(F,C,E,A,D), C:(A,B,F,E,D), D:(A,B,C,E,F) E:(A,B,C,F,D) F:(E,A,C,B,D)

3. Implemented in src/python/hw5.py (run with ./run.sh)

4. I don't think that it matters who goes first in the scenarios we have chosen with complete preference bipartite graphs. This seems like a very tightly constrained example of a matching problem though, and I would imagine that chaning the imputs or definition of stability could easily result in order becoming deterministic.