## Packages ##
import sys
import numpy as np
import copy
import pandas as pd

## Functions ##
# Depth first search
def dfs(graph, start):
    visited, stack = set(), [start]
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.add(vertex)
            stack.extend(graph[vertex] - visited)
    return visited
# Breadth first search
def bfs(graph, start):
	visited, queue = set(), [start]
	while queue:
		vertex = queue.pop(0)
		if vertex not in visited:
			visited.add(vertex)
			queue.extend(graph[vertex] - visited)
	return visited
# Read/parse tsv DIMACS into simple graph with integer vertices
def parse_dimacs(inFile, weighted=False):
	# Read Input
	with open(inFile) as f:
		lines = f.read().splitlines()
	# split lines on tab
	def line_parse(line):
		lArray = line.split('\t')
		return(tuple(map(lambda x: int(x), lArray)))
	lines = list(map(line_parse, lines))
	# Initialize graph
	graph = Graph()
	# Knowing DIMACS, parse out n,m and delete from edges
	graph.set_n(lines[0][0])
	m = lines[0][1] # This might be important to check work, but I'm going to ignore it for now
	del lines[0]
	# Add edges from input file
	graph.add_edges(lines)
	# return the graph as a final output
	if weighted: return(graph,lines) 
	return graph
# Read/parse tsv weighted graph
def parse_weighted(inFile):
	graph,lines = parse_dimacs(inFile, weighted=True)
	graph = WeightedGraph(graph, lines)
	#return the weighted graph as a final output
	return graph
# Read/parse tsv weighted graph
def parse_directed(inFile):
	graph,lines = parse_dimacs(inFile, weighted=True)
	graph = DirectedGraph(graph, lines)
	#return the weighted graph as a final output
	return graph
def parse_rooted(inFile):
	graph,lines = parse_dimacs(inFile, weighted=True)
	graph = RootedGraph(graph, lines)
	#return the rooted graph as a final output
	return graph
## End Functions ##

## classes ##
# Ajacency Matrix class to store relations between vertices of a graph
class AdjacencyMatrix:
	# Function to construct adjacency matrix from simple graph
	def construct_adj_matrix(self,graph):
		adjMatrix = np.zeros((max(list(graph.graphD.keys()))+1,max(list(graph.graphD.keys()))+1))
		for key, values in graph.get_graph().items():
			for value in values:
				adjMatrix[key, value]=1
				adjMatrix[value, key]=1
		return(adjMatrix)
	# Initialize
	def __init__(self, adjMat=None,n=None,simpleGraph=None):
		if adjMat is not None:
			self.adjMat=adjMat
		if simpleGraph is None and n is None:
			raise ValueError('Must supply a numeber of vertices for n*n or graph.')
		if simpleGraph is None and n is not None:
			self.adjMat = np.zeros((n,n))
		if simpleGraph is not None:
			self.adjMat = self.construct_adj_matrix(simpleGraph)
	# Method to return the matrix
	def get_matrix(self):
		return(self.adjMat)

# Graph class to store vertices, edges and methods for oppearting on simple, undirected, graph
class Graph:
	# Initialize
	def __init__(self, graphD=None,n=None,m=None, adjacencyMatrix=None):
		if graphD is None:
			graphD = {}
			n = 0
			m = 0
		self.graphD = graphD
		self.n = n
		self.m = m
		self.adjacencyMatrix=adjacencyMatrix
	# Method for adding n
	def add_vtx(self, vtx):
		if vtx not in self.graphD:
			self.graphD[vtx] = set()
			self.set_n()
	# Method for showing vertices[int]
	def get_vtxs(self):
        	return list(self.graphD.keys())
	# 'Private' method for adding edge[tuple(int,int)]
	def __add_edge(self, edge):
		if edge[0] not in self.graphD.keys():
			self.add_vtx(edge[0])
		if edge[1] not in self.graphD.keys():
			self.add_vtx(edge[1])
		self.graphD[edge[0]].add(edge[1])
		self.graphD[edge[1]].add(edge[0])
		#if edge[1] not in self.graphD[edge[0]]:
		#	self.graphD[edge[0]].append(edge[1])
	# Exposed method for adding edges
	def add_edges(self, edges):
		for edge in edges: self.__add_edge(edge)
		self.construct_adj_matrix()
		self.set_m()
	# Method for showing edges
	def get_graph(self):
        	return self.graphD
	# Set n vertices
	def set_n(self,n=None):
		if n is not None: 
			# Use knowledge from DIMACS to make sure all vertices are in graph
			for i in range(0, n): self.add_vtx(i)
			self.n = n
		self.n = len(self.graphD.keys())
	# Get n vertices
	def get_n(self,n=None):
		return self.n
	# Set m edges
	def set_m(self,m=None):
		if m is not None:
			self.m = m
			return None
		self.m = sum(sum(self.get_adj_matrix())/2)
	# Get m edges
	def get_m(self,n=None):
		return self.m
	# Method for computing density of graph
	def get_density(self):
		return  self.m/(self.n*(self.n-1)/2)
	# Method for computing max degree
	def max_degree(self):
		return int(max(sum(self.get_adj_matrix())))
	# Method for computing min degree vtx
	def min_degree(self):
		return int(min(sum(self.get_adj_matrix())))
	def vtx_degrees(self):
		vtxD={}
		for i, s in enumerate(list(sum(self.get_adj_matrix()))):
			vtxD[i]=s
		return vtxD
	# Method for adding adjacency matrix to the graph
	def construct_adj_matrix(self):
		self.adjacencyMatrix = AdjacencyMatrix(simpleGraph=self)
	# Method for getting adjacency matrix
	def get_adj_matrix(self):
		if self.adjacencyMatrix is None:
			raise ValueError('Still need to compute adjacency matrix.')
		return self.adjacencyMatrix.get_matrix()
	# Method to Check connectivity
	def is_connected(self):
		vtxs=set(self.get_vtxs())
		#if(len(vtxs)<1): return False
		v=vtxs.pop()
		r=dfs(self.graphD, v)
		vtxs=vtxs-r
		if(len(vtxs)==0): return True
		return False
	# Method to remove vtx
	def remove_vtx(self, vtx):
		self.graphD.pop(vtx, None)
		for key, value in self.graphD.items():
			self.graphD[key] = value - {vtx}
		return None
	# Method to remove edge
	def remove_edge(self, edge):
		if(edge[0] in self.graphD.keys()): self.graphD[edge[0]]=self.graphD[edge[0]]-{edge[1]}
		if(edge[1] in self.graphD.keys()): self.graphD[edge[1]]=self.graphD[edge[1]]-{edge[0]}
		self.construct_adj_matrix()
		return None
	def get_edges(self):
		gEdges=set()
		w=np.where(self.get_adj_matrix())
		for i in list(range(len(w[0]))):
			if w[0][i]<w[1][i]: gEdges.add((w[0][i],w[1][i]))
			else: gEdges.add((w[1][i],w[0][i]))
		return sorted(gEdges)
	# Method to contract an edge
	def contract_edge(self, edge):
		# Get all of the connections to the two vertices
		neighbors = set()
		# for v in np.argwhere(self.get_adj_matrix()[edge[0]]==1):
		# 	neighbors.add(v[0])
		for v in np.argwhere(self.get_adj_matrix()[edge[1]]==1):
			neighbors.add(v[0])
		neighbors=neighbors-{edge[0],edge[1]}

		# Remove the edge
		self.remove_edge(edge)

		# Remove the vertices
		self.remove_vtx(edge[1])

		# Add back the first vertex and connect it to all of the collected connections
		edgesToAdd = [(edge[0],v) for v in neighbors]
		self.add_edges(edgesToAdd)
		return None
	# Method to copy graph
	def copy(self):
		return copy.deepcopy(self)
	#Method to rename graph with minimum indexing fr adj.mat comparisons


	def minimum_rename(self, removedvtx, movedvtx):
		vtxNames = np.array(sorted(list(self.get_graph().keys())))
		tDict={}
		for v in vtxNames[vtxNames>removedvtx]:
			tDict[v] = v-1
		for k,v in tDict.items():
			newVtxs = set()
			for vert in self.graphD[k]:
				if vert!=movedvtx and vert!=removedvtx:
					if vert in tDict.keys():
						newVtxs.add(tDict[vert])
					else: newVtxs.add(vert)
			self.graphD[v]=newVtxs
		return None
	def minimum_rename_vtx(self, removedvtx):
		vtxNames = np.array(sorted(list(self.get_graph().keys())))
		vtxDict={}
		for v in vtxNames:
			if(v>removedvtx):
				vtxDict[v]=v-1
			else: vtxDict[v]=v
		self.add_vtx(removedvtx)
		gCopy=self.graphD
		for k, vals in gCopy.items():
			if k!=removedvtx:
				self.graphD[vtxDict[k]]=set([vtxDict[vtx] for vtx in vals])

		m = list(self.graphD.keys())[np.argmax(np.array(list(self.graphD.keys())))]
		self.graphD.pop(m, None)

class WeightedGraph:
	# Initialize
	def __init__(self, graph, lines):
		self.graph=graph
		self.weightMat=self.graph.get_adj_matrix()
		for line in lines:
			self.weightMat[line[0],line[1]]=line[2]
			self.weightMat[line[1],line[0]]=line[2]
		self.weightMat = pd.DataFrame(self.weightMat)
		self.weightMat = self.weightMat.astype(int)
	def get_weight_mat(self):
		return self.weightMat
		
class DirectedGraph:
	# Initialize
	def __init__(self, graph, lines):
		self.graph=graph
		self.weightMat=self.graph.get_adj_matrix()
		for line in lines:
			self.weightMat[line[1],line[0]]=line[2]
		self.weightMat = pd.DataFrame(self.weightMat)
		self.weightMat = self.weightMat.astype(int)
	def get_weight_mat(self):
		return self.weightMat

class RootedGraph:
	# Method to add root to graph
	def __init__(self, graph, lines):
		self.graph=graph
		self.linRoot=dict.fromkeys(self.graph.get_vtxs())
		for k in self.linRoot.keys():
			self.linRoot[k] = ''
		self.rootMat=self.graph.get_adj_matrix()
		for line in lines:
			if(line[1]>line[0]): self.rootMat[line[1],line[0]]=1
			else: self.rootMat[line[0],line[1]]=1
	def root(self):
		self.root = dict.fromkeys(self.graph.get_vtxs())
		for k in self.root.keys():
			self.root[k] = []
		return self

	def name(self):
		return ''.join(sorted(list(self.linRoot.values())))