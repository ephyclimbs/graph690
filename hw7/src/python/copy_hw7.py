import base
import sys
import numpy as np
import copy
import itertools

found_k5=False
found_k33=False
adjacency_matrices=set()

def is_k5(g):
	return sum(sum(g.get_adj_matrix()))==20 and len(g.get_vtxs())==5

def is_k33(g):
	if len(g.get_vtxs())==6 and np.array(sum(g.get_adj_matrix())==3).all():
		vtxs=set(np.where(g.get_adj_matrix()==1)[0])
		connections = list(itertools.combinations(vtxs, 2))
		l = [conn for conn in connections if g.get_adj_matrix()[conn[0]][conn[1]]==1]
		for vtx in vtxs:
			s0=0
			s1=0
			for i in l:
				if i[0]==vtx: s0+=1
				if i[1]==vtx: s1+=1
			if s0==3 and s1!=0: return False
			return True
	return False

def have_seen(mat):
	global adjacency_matrices

	if mat in adjacency_matrices: return True
	adjacency_matrices.add(mat)

	return False

def planarity_test(g, level=0):
	global found_k5
	global found_k33

	if not have_seen(str(g.get_adj_matrix())): 
		if is_k5(g):
			found_k5 = True
			return
		else:
			if is_k33(g):
				found_k33 = True
				return

		if found_k5 and found_k33: return

		if len(g.get_vtxs())>4:
			for v in g.get_vtxs():
				gPrime = g.copy()
				gPrime.remove_vtx(v)
				planarity_test(gPrime, level+1)

		if len(g.get_edges())>9:
			for e in g.get_edges():
				gPrime = g.copy()
				gPrime.remove_edge(e)
				planarity_test(gPrime, level+1)

			for e in g.get_edges():
				gPrime = g.copy()
				gPrime.contract_edge(e)
				planarity_test(gPrime, level+1)
	
	return


planarity_test(base.parse_dimacs(sys.argv[1]))

print(not found_k5 or not found_k33)
if found_k5: print('Found K5')
if found_k33: print('Found K3,3')