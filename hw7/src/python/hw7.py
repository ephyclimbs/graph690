import base
import sys
import numpy as np
import copy
import itertools

found_k5=False
found_k33=False
adjacency_matrices=set()

def get_vtxs_from_mat(mat):
	return list(range(len(mat)))

def remove_vtx_from_mat(mat, v):
	mat = np.delete(mat, v, axis=0)
	mat = np.delete(mat, v, axis=1)
	return mat

def remove_edge_from_mat(mat, edge):
	mat[edge[0]][edge[1]]=0
	mat[edge[1]][edge[0]]=0
	return(mat)

def get_edges_from_mat(mat):
	r=np.where(mat==1)
	edges=set()
	for i in list(range(len(r[0]))):
		if r[0][i]<r[1][i]:
			edges.add((r[0][i],r[1][i]))
		else:
			edges.add((r[1][i],r[0][i]))
	return(edges)

def contract_edge_from_mat(mat, edge):
	neighbors=[i for i,x in enumerate(mat[edge[1]]) if x==1]
	for neighbor in neighbors:
		mat[edge[0]][neighbor]=1
		mat[neighbor][edge[0]]=1
	mat=remove_vtx_from_mat(mat, edge[1])
	return(mat)

def is_k5(mat):
	return sum(sum(mat))==20 and len(set(np.where(mat==1)[0]))==5

def is_k33(mat):
	vtxs=set(np.where(mat==1)[0])
	if len(vtxs)==6 and np.array(sum(mat)==3).all():
		connections = list(itertools.combinations(vtxs, 2))
		l = [conn for conn in connections if mat[conn[0]][conn[1]]==1]
		for vtx in vtxs:
			s0=0
			s1=0
			for i in l:
				if i[0]==vtx: s0+=1
				if i[1]==vtx: s1+=1
			if s0==3 and s1!=0: return False
		return True
	return False

def have_seen(strmat):
	global adjacency_matrices

	if strmat in adjacency_matrices: return True
	adjacency_matrices.add(strmat)

	return False

def planarity_test(mat):
	global found_k5
	global found_k33

	if not have_seen(mat.tostring()): 
		if is_k5(mat):
			found_k5 = True
			return
		else:
			if is_k33(mat):
				found_k33 = True
				return

		if found_k5 and found_k33: return

		for v in get_vtxs_from_mat(mat):
			mPrime = copy.deepcopy(mat)
			mPrime = remove_vtx_from_mat(mat, v)
			if len(get_vtxs_from_mat(mPrime))>5: planarity_test(mPrime)

		for e in get_edges_from_mat(mat):
			mPrime = copy.deepcopy(mat)
			mPrime = remove_edge_from_mat(mPrime, e)
			if len(get_edges_from_mat(mPrime))>10: planarity_test(mPrime)

		for e in get_edges_from_mat(mat):
			mPrime = copy.deepcopy(mat)
			mPrime = contract_edge_from_mat(mPrime, e)
			if len(get_edges_from_mat(mPrime))>10: planarity_test(mPrime)

	return


planarity_test(base.parse_dimacs(sys.argv[1]).get_adj_matrix())

print(not found_k5 or not found_k33)
if found_k5: print('Found K5')
if found_k33: print('Found K3,3')