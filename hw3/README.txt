My findings from benchmarking against the homework 2 graphs showed that, of the deterministic algorithms, sorting by descending vertex degree
was the best method. Min Random was the best method overall, but it also takes a heck of a lot of computation. Simple random selection worked pretty
well. So, all-in-all, if I had very little time, but wanted a pretty efficient coloring, I would probably choose descending vertex degree, if I could
do a bit more compute, a few iterations of random, then Min Random if I really wanted te best coloring. 
