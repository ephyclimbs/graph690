#!/bin/bash

echo "****************InOrder"
ls ../hw2/data/test_graphs | xargs -i time python src/python/hw3.py in_order ../hw2/data/test_graphs/{} True

echo "****************Random"
ls ../hw2/data/test_graphs | xargs -i time python src/python/hw3.py random ../hw2/data/test_graphs/{} True

echo "*****************Ascending"
ls ../hw2/data/test_graphs | xargs -i time python src/python/hw3.py ascending ../hw2/data/test_graphs/{} True

echo "*****************Descending"
ls ../hw2/data/test_graphs | xargs -i time python src/python/hw3.py descending ../hw2/data/test_graphs/{} True

echo "*****************Min"
ls ../hw2/data/test_graphs | xargs -i time python src/python/hw3_multi.py min_random ../hw2/data/test_graphs/{} True

