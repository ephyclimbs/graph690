import base
import sys
import random
from datetime import datetime
import numpy
import copy

graph = base.parse_dimacs(sys.argv[2])
quiet = sys.argv[3]

def sort_vertex_order(d, rev):
	if rev: 
		vals=numpy.argsort(numpy.array(list(d.values()))) 
	else: 
		vals=numpy.argsort(-numpy.array(list(d.values())))
	return [list(d.keys())[i] for i in vals]

def in_order(graph):
	return list(sorted(graph.get_vtxs()))
def asc(graph):
	return sort_vertex_order(graph.vtx_degrees(), True)
def desc(graph):
	return sort_vertex_order(graph.vtx_degrees(), False)
def rand(graph):
	random.seed(datetime.now())
	return list(random.sample(graph.get_vtxs(), k=len(graph.get_vtxs())))
def min_rand(graph):
	vtxs = []
	for i in graph.get_vtxs():
		vtxs.append(list(random.sample(graph.get_vtxs(), k=len(graph.get_vtxs()))))
	return vtxs

ordering_switch = {
	'in_order': in_order,
	'ascending': asc,
	'descending': desc,
	'random':rand,
	'min_random':min_rand
	}

def search_gc(vtxs, graph):
	temp_graph=graph.copy()
	colorD = {0:[],1:[]}
	colorD[0]=vtxs.pop(0)
	temp_graph.remove_vtx(colorD[0])
	while vtxs:
		colorD[1].append(vtxs.pop(0))
	# Check if these vertices are still connected and then add a color class 
	search = len(colorD[1])
	while temp_graph.is_connected() and search>1:
		search=search-1
		dl = len(colorD)-1
		nex = colorD[dl].pop(0)
		colorD[dl+1]=colorD[dl]
		colorD[dl]=nex
		temp_graph.remove_vtx(nex)
	return colorD


def greedy_color(graph, ordering):
	vtxs = ordering_switch[ordering](graph)
	# Do one thing for non min_random, or iterate for min_random
	if isinstance(vtxs[0], int):
		colorD_fin=search_gc(vtxs, graph)	
	else:
		smallest_dict=len(graph.get_vtxs())
		for vtxL in vtxs:
			vtxL=list(vtxL)
			colorD=search_gc(vtxL, graph)
			if len(colorD.keys())<smallest_dict:
				colorD_fin=colorD
				smallest_dict=len(colorD_fin.keys())
	return colorD_fin

gc = greedy_color(graph, sys.argv[1])
print(len(gc.keys()))
if quiet=='False': print(gc)
