import base
import sys
import random
from datetime import datetime
import numpy
import copy

graph = base.parse_dimacs(sys.argv[2])
quiet = sys.argv[3]

def sort_vertex_order(d, rev):
	if rev: 
		vals=numpy.argsort(numpy.array(list(d.values()))) 
	else: 
		vals=numpy.argsort(-numpy.array(list(d.values())))
	return [list(d.keys())[i] for i in vals]

def in_order(graph):
	return list(sorted(graph.get_vtxs()))
def asc(graph):
	return sort_vertex_order(graph.vtx_degrees(), True)
def desc(graph):
	return sort_vertex_order(graph.vtx_degrees(), False)
def rand(graph):
	random.seed(datetime.now())
	return list(random.sample(graph.get_vtxs(), k=len(graph.get_vtxs())))
def min_rand(graph):
	vtxs = []
	for i in graph.get_vtxs():
		random.seed(datetime.now())
		vtxs.append(list(random.sample(graph.get_vtxs(), k=len(graph.get_vtxs()))))
	return vtxs

ordering_switch = {
	'in_order': in_order,
	'ascending': asc,
	'descending': desc,
	'random':rand,
	'min_random':min_rand
	}

def search_gc(vtxs, graph):
	adj_mat = graph.get_adj_matrix()
	colorD={1:{vtxs.pop(0)}}
	while vtxs:
		vtx=vtxs.pop(0)
		keys=set(colorD.keys())
		searching = True
		found = False
		while keys and searching:
			key = keys.pop()
			clear=True
			for checkVtxs in colorD[key]:
				if adj_mat[checkVtxs,vtx]==1: clear=False
			if clear == True: 
				colorD[key].add(vtx)
				found=True
				searching=False
		if not found: colorD[len(colorD.keys())+1]={vtx}
	return colorD



def greedy_color(graph, ordering):
	vtxs = ordering_switch[ordering](graph)
	# Do one thing for non min_random, or iterate for min_random
	if isinstance(vtxs[0], int):
		colorD_fin=search_gc(vtxs, graph)	
	else:
		smallest_dict=len(graph.get_vtxs())
		for vtxL in vtxs:
			vtxL=list(vtxL)
			colorD=search_gc(vtxL, graph)
			if len(colorD.keys())<smallest_dict:
				colorD_fin=colorD
				smallest_dict=len(colorD_fin.keys())
	return colorD_fin

gc = greedy_color(graph, sys.argv[1])
print(len(gc.keys()))
if quiet=='False': print(gc)
