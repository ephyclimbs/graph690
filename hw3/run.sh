#!/bin/bash

echo "In Order"
python src/python/hw3.py in_order data/graph3.txt False
echo "Random"
python src/python/hw3.py random data/graph3.txt False
echo "Ascending"
python src/python/hw3.py ascending data/graph3.txt False
echo "Descending"
python src/python/hw3.py descending data/graph3.txt False
echo "Min"
python src/python/hw3.py min_random data/graph3.txt False
echo "Min_Multi"
python src/python/hw3_multi.py min_random data/graph3.txt False