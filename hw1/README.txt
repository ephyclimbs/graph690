1. Naive algorithm to Find a K3 clique in a graph
First, it seems like efficeiency probably depends on the density of the graph. It also seems like you could tackle this from many diferent representaions of a graph. To take one approach:
Using an n*n binary adjacency matrix:
	1. sum the rows and filter rows and columns by rowsums>=2
	2. Take all possible 3x3 matrices with same row and column indeces, order independent, and see if sum(submatrix)/2==3,
		if true then the row/col index refer to the vertices of a K3 cliue.
I think my solution would run in nCr polynomial time. It could be parallelized though, which is how I'm used to dealing with big computations...

2. Description of python project
This is a python 3.7.2 project written by Ephraim Love. Requires: numpy
To run on graph1.txt:
	./run.sh
To run on a new graph:
	python src/python/min_max_connect.py "PATH TO YOUR GRAPH"

3. Disclosures
I'm no computer scientist. I decided to look at some examples of commonly used data structures to hold graphs. We haven't gone through many examples yet, but the structure I chose was a ballance of what I saw in a very simple example on "TutorialsPoint" and what I thought would be helpful.

4. Notes to Aaron
I like having a simple store of edges that I think will be easy to ammend to work with a directed graph as well, and a sepperate class for the adjacency matrix. I'm guessing in the long run I will want these sepperate. Please don't jusge my terrible programming skills, I am but a noob. Also, I did begin to implement this in C. It didn't go well, so maybe eventually I'll get to that, but for now... python.
