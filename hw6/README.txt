1. Implemented in src/python/hw6.py

2. Basically, you can just run a breadth first search, and pick the center of the graph. Root from the center. At most a tree will have two centers, and isomorphic trees will have the same number of centers, so at most you will have two calls to the algorithm. (https://logic.pdmi.ras.ru/~smal/files/smal_jass08_slides.pdf)

3. 1)March 6 
   2)March 13
   3)March 27
