import base
import sys

# Function to take a rooted graph and assign a canonical name based on vertices names
def is_leaf(rootedGraph, v):
	if len(rootedGraph.root)==0: return True
	return False

def collect_children(r, i):
	searching=True
	while searching:
		vec =  [a for a,x in enumerate(r.rootMat[i]) if x==1 and a not in r.root[i] and i not in r.root[a]]
		for v in vec:
			if v not in r.root[i] and i not in r.root[v]: r.root[i].append(v)
			r = collect_children(r, v)
		searching = len(vec)>0
	return r

def assign_canonical_names(r, v):
	if is_leaf(r, v):
		r.linRoot[v]='01'
	else:
		for x in r.root[v]:
			assign_canonical_names(r, x)
		t = ['0']
		temp = []
		for a in r.root[v]:
			temp.append(str(r.linRoot[a]))
		temp = sorted(temp)
		for ti in temp:
			t.append(ti)
		t.append('1')
		r.linRoot[v] =''.join(t)
	return None

def ahu_tree_isomorphism(t1 , t2):
	r1 = base.parse_rooted(t1).root()
	for i in range(len(r1.rootMat)):
	 	r1 = collect_children(r1, i)
	
	r2  =  base.parse_rooted(t2).root()
	for i in range(len(r2.rootMat)):
	 	r2 = collect_children(r2, i)


	assign_canonical_names(r1, 0)
	assign_canonical_names(r2, 0)



	return r1.name() == r2.name()

print(ahu_tree_isomorphism(sys.argv[1],sys.argv[2]))