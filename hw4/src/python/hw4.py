import base
import sys
import itertools

wgraph = base.parse_weighted(sys.argv[1])

# Check for initiating vertex argument
if(len(sys.argv)==3): 
	start_vtx=int(sys.argv[2])
else: start_vtx=0

def greedy_tsp(wgraph, sv=start_vtx):
	r = [sv]
	secondSearch=True
	m = wgraph.get_weight_mat()
	m[m==0]=sum(wgraph.get_weight_mat().sum()) # So we can take minarg
	f=0
	while f < len(wgraph.graph.get_vtxs())-1:
		cr = r[len(r)-1]
		temp_m = m
		for ind in temp_m.index:
			if ind in r: temp_m = temp_m.drop(ind, axis=0)
		w=list(temp_m[cr])
		n=temp_m.index
		for i,x in enumerate(n):
			if w[i]==min(w): 
				nextI=x
				break
		r.append(nextI)
		f+=1
	r.append(r[0])
	return r


def add_up_weights(wgraph, gtss):
	solveWeight=0
	for i in list(range(0,len(gtss)-1)):
		solveWeight+=wgraph.get_weight_mat()[gtss[i]][gtss[i+1]]
	return solveWeight


def optimal_tsp(wgraph):
	best_solve=sum(wgraph.get_weight_mat().sum())
	for v in list(itertools.permutations(wgraph.graph.get_vtxs())):
		v=list(v)
		v.append(v[0])
		ns = add_up_weights(wgraph, v)
		if(ns<best_solve): 
			gtsp=v
			best_solve=ns
	return gtsp


# Greedy TSP solver
gtsp = greedy_tsp(wgraph)

# Less Greedy TSP solver 
#gtsp = optimal_tsp(wgraph)

print(gtsp)
print(add_up_weights(wgraph, gtsp))


