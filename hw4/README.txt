1. Name a linear-time-computable neccessary condition for each of the following (after checking connectivity):
	a) Eulerian path - check that there are two odd degree vertices
	b) Eularian cycle - check that all vertices have an even degree
	c) Hamiltonian path - check that there are fewer than 3 1-degree vertices

2. Greedy tsp solver can be found in src/python/hw4.py

3. The optimal solver (by permutation method) for tsp can be found in src/python/optimal_sol.py

4.  a) I did this with ./benchmark.sh &> benchmark.log
	b) I munged the log file into a plot, some metrics and a csv with R
	c) I found that the best predictor of how bad the solution was seemed to be graph order. This makes sense, because the aproximate solver has more opportunities to make bad choices. It makes bad choices particularly in situations where the graph fails the triangle inequality, so the graph order may be a spurious correlation, but both factors are likely important.
	d) The aproximate solution is definitely bounded. Since the greedy solution uses a deterministic, flawed logic it will only find an optimal solution by chance.
	e) I made a graph in data/bad.txt to show that the lack of optimality is unbounded. Because edge weights can be seen as a random variable, we can trick a greedy solution into boxing itself into taking a path of arbitrary inefficiency. I got it up to thousands of times worse...
