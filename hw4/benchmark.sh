#!/bin/bash

ls ../hw4/data/tsp_graphs | xargs -i python src/python/hw4.py ../hw4/data/tsp_graphs/{}

echo "**********"

ls ../hw4/data/tsp_graphs | xargs -i python src/python/optimal_sol.py ../hw4/data/tsp_graphs/{}
